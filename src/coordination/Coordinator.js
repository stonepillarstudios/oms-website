import React from "react";
import Home from "../home/Home";
import Admin from "../admin/Admin";
import VotingScreen from "../voting/VotingScreen";
import NotFound from "../other/NotFound";
import {BrowserRouter as Router, Route, Switch, withRouter} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {Drawer, IconButton, List, ListItemIcon, ListItem, ListItemText, Divider} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import HomeIcon from '@material-ui/icons/Home';
import HowToVoteIcon from '@material-ui/icons/HowToVote';
import AssessmentIcon from '@material-ui/icons/Assessment';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import FeedbackIcon from '@material-ui/icons/Feedback';
import PeopleIcon from '@material-ui/icons/People';
import InfoIcon from '@material-ui/icons/Info';
import Typography from '@material-ui/core/Typography';
import Login from "./Login";
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";
import './Coordinator.css';
import Legal from "../other/Legal";
import SuggestionScreen from "../suggestion/SuggestionScreen";
import HistoryIcon from '@material-ui/icons/History';
import SummariesScreen from "../summaries/SummariesScreen";
import CommitteeScreen from "../committee/CommitteeScreen";
import AboutUsScreen from "../about_us/AboutUsScreen";


export default class Coordinator extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            loggedInUser: null,
            drawerOpen: false,
        };

        this.onLogin = this.onLogin.bind(this);
        this.toggleDrawer = this.toggleDrawer.bind(this);
    }

    onLogin(usr){
        this.setState({
            loggedInUser: usr
        });
    }

    onRoute(routerprops, dest){
        return () => {
            routerprops.history.push(dest);
            this.toggleDrawer(false)();
        }
    }

    toggleDrawer(newState){
        return () => {
            this.setState({
                drawerOpen: newState,
            });
        }
    }

    render() {

        const theme = createMuiTheme({
            palette: {
                primary: {
                    main: '#306f6a',
                },
                secondary: {
                    main: '#4d306f',
                },
                background: {
                    default: '#30356f',
                },
            }
        });

        const links = [
            ['Home', '/', <HomeIcon/>],
            ['About Us', '/about', <InfoIcon/>],
            ['Vote', '/vote', <HowToVoteIcon/>],
            ['Suggest a topic', '/suggest', <FeedbackIcon/>],
            ['Past events', '/past-events', <HistoryIcon/>],
            ['Executive Committee', '/committee', <PeopleIcon/>],
            ['Admin Console', '/admin', <AssessmentIcon/>],
            ['Privacy Policy', '/privacy', <AccountBalanceIcon/>]
        ];

        return (<MuiThemeProvider theme={theme}>
            <Router>
            <AppBar position="sticky">
                <Toolbar>
                    <Typography variant="h4" style={{flexGrow: 1, fontFamily: 'inherit'}}><strong>Open Mind Society</strong></Typography>

                    <Login onLogin={this.onLogin}/>
                    <IconButton color="inherit" onClick={this.toggleDrawer(true)}><MenuIcon/></IconButton>
                    <Drawer anchor="right" open={this.state.drawerOpen} onClose={this.toggleDrawer(false)}>
                        <div role="presentation">
                            <List>
                                {this.state.loggedInUser && <div><ListItem>
                                    <ListItemIcon><AccountCircleIcon/></ListItemIcon>
                                    <ListItemText primary={this.state.loggedInUser.displayName}/>
                                </ListItem><Divider/>
                                </div>}
                                {React.createElement(withRouter(routerProps => {
                                    const curPath = routerProps.location.pathname;
                                    return (<div>
                                        {links.map(linkfo => (
                                            <ListItem button key={linkfo[1]} disabled={curPath === linkfo[1]}
                                                      onClick={this.onRoute(routerProps, linkfo[1])}>
                                            <ListItemIcon>{linkfo[2]}</ListItemIcon>
                                            <ListItemText primary={linkfo[0]}/>
                                        </ListItem>))}
                                    </div>);
                                }))}

                            </List>
                        </div>
                    </Drawer>
                </Toolbar>
            </AppBar>
            <Switch>
                <Route exact path="/" render = {(props) =>
                    <Home {...props} loggedInUser={this.state.loggedInUser}/>}/>
                <Route exact path="/about" render={(props) =>
                    <AboutUsScreen {...props} /> } />
                <Route exact path="/admin" render={(props) =>
                    <Admin {...props} loggedInUser={this.state.loggedInUser}/>}/>
                <Route exact path="/vote" render={(props)=>
                    <VotingScreen {...props} loggedInUser={this.state.loggedInUser}/>}/>
                <Route exact path="/suggest" render={(props)=>
                    <SuggestionScreen {...props} loggedInUser={this.state.loggedInUser}/>}/>
                <Route exact path="/past-events" render={(props)=>
                    <SummariesScreen {...props} />}/>
                <Route exact path="/committee" render={(props)=>
                    <CommitteeScreen {...props} />}/>
                <Route exact path="/privacy" render={(props)=>
                    <Legal {...props} />}/>
                <Route component={NotFound}/>
            </Switch>
            </Router>
        </MuiThemeProvider>);
    }

}
