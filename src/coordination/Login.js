import React from "react";
import firebase from "firebase";
import {analytics, firestore} from "../Firebase";
import Button from "@material-ui/core/Button";
import User from "../data_classes/User";
import {Card, Fade, Modal} from "@material-ui/core";
import Backdrop from '@material-ui/core/Backdrop';

export default class Login extends React.Component {

    constructor(props){
        super(props);
        //props: onLogin

        this.state = {
            checking: true,
            loggedInUser: null,
            showPrepModal: false,
        };

        this.gauth = new firebase.auth.GoogleAuthProvider();
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);

        this.onLoginClick = this.onLoginClick.bind(this);
        this.kickOffLogin = this.kickOffLogin.bind(this);

        this.unsub = null;
    }

    componentDidMount() {
        // subscrizzle
        this.unsub = firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.uploadUser(user).then(usr=>{
                    this.setState({loggedInUser: usr});
                    analytics.setUserProperties({role: usr.role});
                    this.props.onLogin(usr);
                });

                analytics.setUserId(user.uid);
                const newUser = user.metadata.creationTime === user.metadata.lastSignInTime;
                analytics.logEvent(newUser ? 'sign_up' : 'login',
                    {method: user.providerData[0].providerId});
            }
            this.setState({checking: false});
        });
    }

    componentWillUnmount() {
        this.unsub();
    }

    onLoginClick(){
        if (this.state.loggedInUser){
            firebase.auth().signOut().then(x=>{
                this.setState({
                    loggedInUser: null
                });
                this.props.onLogin(null); //signal logout
            }); return;
        }

        this.setState({checking: true, showPrepModal: true});

    }

    kickOffLogin(){
        this.setState({showPrepModal: false});
        firebase.auth().signInWithPopup(this.gauth).then(result => {
            // sign in success - handled with another handler
        }).catch(err => {
            console.log(err.message);
            this.setState({checking: false});
        });
    }

    uploadUser(details){
        return new Promise((resolve, reject)=>{
            firestore.collection('users').doc(details.uid).get().then(doc => {
                if (doc.exists) {
                    const loggedInUser = new User();
                    loggedInUser.populateWithDoc(doc);
                    resolve(loggedInUser);
                } else {
                    const role = (details.email.endsWith('@uct.ac.za') || details.email.endsWith('@myuct.ac.za')) ? 'uctcom' : 'none';
                    firestore.collection('users').doc(details.uid).set({
                        displayName: details.displayName,
                        email: details.email,
                        creation: new Date(),
                        role: role
                    }).then(() => {
                        const loggedInUser = new User();
                        loggedInUser.populate(details.uid, details.email, details.displayName, role);
                        resolve(loggedInUser);
                    });
                }
            }).catch(reject);
        });
    }

    render() {

        const modalClick = uct => () => {
            this.setState({
                showPrepModal: false
            });
            if (uct){
                this.gauth.setCustomParameters({
                    hd: 'uct.ac.za',
                });
            }else{
                this.gauth.setCustomParameters({
                    prompt: 'select_account',
                    hd: undefined,
                });
            }
            this.kickOffLogin();
        };

        return (<div>
            <Button color="inherit" variant="outlined" onClick={this.onLoginClick} disabled={this.state.checking} >
                {this.state.loggedInUser ? 'Logout' : 'Login'}
            </Button>
            <Modal open={this.state.showPrepModal} onClose={()=>this.setState({showPrepModal: false, checking: false})}
                   closeAfterTransition BackdropComponent={Backdrop} BackdropProps={{timeout: 700}}>
                <Fade in={this.state.showPrepModal}><Card className="modal">
                    <h2>Are you UCT?</h2>
                    <p>Members of the UCT community have access to more website features and society privileges, like
                        voting for and submitting topics.</p>
                    <Button variant="text" onClick={modalClick(true)}>UCT</Button>
                    <Button variant="text" onClick={modalClick(false)}>Non-UCT</Button>
                </Card></Fade>
            </Modal>
        </div>);
    }

}
