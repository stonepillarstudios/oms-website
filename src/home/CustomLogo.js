import React from "react";

export default class CustomLogo extends React.Component { //todo make variable-size and place on toolbar

    constructor(props){
        super(props);

        this.state = {
            botColor: '#1c1f4c',
            topColor: '#57d492'
        };

        this.updateColors = this.updateColors.bind(this);
        this.colourChanger = null;
    }

    componentDidMount() {
        this.updateColors();
        this.colourChanger = setInterval(this.updateColors, 5000);
    }

    componentWillUnmount() {
        clearInterval(this.colourChanger);
    }

    rndHi(){
        return Math.round(Math.random() * 255 * .5 + (255*.5));
    }
    rndLo(){
        return Math.round(Math.random() * 255 * .5);
    }

    updateColors(){
        //some logic to make sure colours look good
        let tc = [0,0,0];
        while(Math.abs(tc[0]-tc[1]) + Math.abs(tc[1]-tc[2]) + Math.abs(tc[2]-tc[0]) < 250){
            tc = [this.rndHi(), this.rndHi(), this.rndHi()];
        }

        let bc = [0,0,0];
        while(Math.abs(bc[0]-bc[1]) + Math.abs(bc[1]-bc[2]) + Math.abs(bc[2]-bc[0]) < 250){
            bc = [this.rndLo(), this.rndLo(), this.rndLo()];
        }

        this.setState({
            topColor: 'rgb('+tc[0]+','+tc[1]+','+tc[2]+')',
            botColor: 'rgb('+bc[0]+','+bc[1]+','+bc[2]+')',
        })
    }

    render() {

        const transitionStyle = {transition: 'fill 5s'};

        return(<div style={{height: '412px', width: '356px', margin: 'auto', position: 'relative'}}>
            <svg xmlns="http://www.w3.org/2000/svg"
                 width="100%" height="100%"
                 viewBox="0 0 1080 1080" style={{position: 'absolute', top: 0, left: 0, zIndex: '3'}}>
                <path id="logo_outerq"
                      fill={this.state.topColor} style={transitionStyle}
                      d="M 527.00,63.00
           C 527.00,63.00 527.00,162.00 527.00,162.00
             544.05,159.75 550.06,159.88 567.00,160.00
             597.47,160.21 627.22,167.86 655.00,180.14
             680.30,191.32 703.45,207.46 723.00,227.00
             793.96,297.94 812.84,403.54 768.74,494.00
             756.25,519.62 739.33,541.15 719.00,560.99
             700.28,579.25 677.75,593.60 654.00,604.30
             626.83,616.56 596.84,623.79 567.00,624.00
             550.06,624.12 544.05,624.25 527.00,622.00
             527.00,622.00 527.00,863.00 527.00,863.00
             527.00,863.00 631.00,863.00 631.00,863.00
             631.00,863.00 631.00,733.00 631.00,733.00
             631.00,730.82 630.78,727.26 632.02,725.43
             634.04,722.44 645.21,720.66 649.00,719.57
             663.22,715.51 682.68,708.51 696.00,702.22
             755.46,674.13 802.55,634.54 838.31,579.00
             912.07,464.44 908.47,314.38 830.28,203.00
             811.58,176.36 788.66,152.43 763.00,132.42
             734.70,110.37 701.91,92.39 668.00,80.66
             641.52,71.50 613.99,64.96 586.00,62.91
             586.00,62.91 574.00,62.00 574.00,62.00
             562.13,61.98 537.43,61.13 527.00,63.00 Z
           M 567.00,921.44
           C 537.19,926.95 514.05,950.93 514.00,982.00
             513.99,990.54 513.68,995.56 516.21,1004.00
             523.58,1028.57 546.72,1048.69 573.00,1049.00
             588.29,1049.18 594.97,1048.96 609.00,1041.74
             646.20,1022.62 653.27,969.85 624.91,940.01
             609.87,924.19 588.16,918.73 567.00,921.44 Z" />
            </svg>

            <svg xmlns="http://www.w3.org/2000/svg"
                 width="100%" height="100%"
                 viewBox="0 0 1080 1080" style={{position: 'absolute', top: '0', left: '0', zIndex: '1'}}>
                <path id="logo_circle"
                      fill={this.state.botColor} style={transitionStyle}
                      d="M 541.00,159.21
           C 509.15,163.97 488.54,168.28 459.00,182.32
             407.81,206.64 364.43,254.00 344.42,307.00
             306.25,408.12 338.78,521.65 428.00,584.28
             445.55,596.60 465.66,606.86 486.00,613.67
             502.81,619.29 529.41,625.79 547.00,626.00
             547.00,626.00 577.00,626.00 577.00,626.00
             610.36,625.95 653.25,611.03 682.00,594.57
             704.65,581.61 725.52,563.97 742.25,544.00
             801.89,472.81 814.23,372.60 773.74,289.00
             762.82,266.45 747.26,245.03 729.83,227.09
             710.32,207.00 685.36,190.81 660.00,179.23
             637.56,168.99 601.67,159.04 577.00,159.21
             577.00,159.21 541.00,159.21 541.00,159.21 Z" />
            </svg>

            <svg xmlns="http://www.w3.org/2000/svg"
                 width="100%" height="100%"
                 viewBox="0 0 1080 1080" style={{position: 'relative', zIndex: '3', top: 0, left: 0}}>
                <path id="logo_cover"
                      fill={this.state.topColor} fillOpacity="0.3" style={transitionStyle}
                      d="M 527.00,161.00
           C 527.00,161.00 527.00,624.00 527.00,624.00
             527.00,624.00 549.00,626.00 549.00,626.00
             549.00,626.00 577.00,626.00 577.00,626.00
             591.37,625.98 614.03,621.41 628.00,617.42
             691.21,599.39 744.98,555.38 773.74,496.00
             811.71,417.61 803.39,323.29 751.85,253.00
             737.09,232.88 720.14,216.78 700.00,202.15
             682.29,189.29 660.77,178.28 640.00,171.33
             622.16,165.37 595.72,159.03 577.00,159.00
             577.00,159.00 548.00,159.00 548.00,159.00
             548.00,159.00 527.00,161.00 527.00,161.00 Z" />
            </svg>
        </div>);
    }

}
