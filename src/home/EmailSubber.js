import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {analytics, firestore} from "../Firebase";

export default class EmailSubber extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            edtEmail: '',
            submitted: false,
            error: null
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(){
        if (this.validateEmail(this.state.edtEmail)) {
            this.setState({submitted: true});

            //upload to firestore
            firestore.collection('subscribers').add({
                email: this.state.edtEmail.replace(/^\s+|\s+$/g, ''), //trim spaces
                stamp: new Date(),
                vula: false,
            });

            analytics.logEvent('email_subscribed');

        }else
            this.setState({error: 'Invalid email'});
    }

    validateEmail(email) {
        const trimmed = email.replace(/^\s+|\s+$/g, '');
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(trimmed).toLowerCase());
    }

    render(){
        return (<div>
            {this.state.submitted &&
            <Paper className="paper_block">Thank you for subscribing to the OMS! You'll get an email as soon as there's news.<br/>
                <br/>If you know anyone that would also be interested, please link them to this site too.</Paper> }
            {!this.state.submitted &&
            <Paper className="paper_block">
                <h3>Subscribe to become a member (completely free)!</h3>
                <Grid container>
                    <Grid item xs={8}>
                        <form>
                            <TextField id="edt_email" label="Email" onChange={event => {
                                this.setState({edtEmail: event.target.value});
                            }} margin="normal" fullWidth variant="outlined" error={this.state.error !== null}
                            helperText={this.state.error}/>
                        </form>
                    </Grid>
                    <Grid item xs={4} style={{paddingTop: '25px'}}>
                        <Button variant="contained" onClick={this.onSubmit} color="primary">Submit</Button>
                    </Grid>
                </Grid>
                <p>If you have one, enter your UCT email to get added to the Vula site.</p>
            </Paper>}
        </div>);
    }

}
