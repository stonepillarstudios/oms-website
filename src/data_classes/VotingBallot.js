import {firestore} from '../Firebase';

export default class VotingBallot {
    /**
     * Create this ballot's skeleton
     * @param doc firebase document containing voting ballot data
     */
    constructor(doc){
        this.id = doc.id;
        const data = doc.data();

        this.deadlineUnix = data.deadline.seconds;

        this.topics = null;
    }

    loadTopics(){ //load or reload
        const self = this;
        return new Promise((resolve, reject) => {
            firestore.collection('voting/'+self.id+'/topics').get().then(snap => {
                let parsed = [];
                snap.forEach(doc => {
                    parsed.push(new Topic(doc));
                });
                self.topics = parsed;
                resolve();
            }).catch(reject);
        });
    }

    hasVoted(userId){
        const self = this;
        return new Promise(((resolve, reject) => {
            firestore.collection('voting/'+self.id+'/votes').where('voter', '==', userId)
                .get().then(snap => {
                resolve(!snap.empty);
            }).catch(reject);
        }));
    }

}

class Topic {
    constructor(doc){
        this.id = doc.id;
        const data = doc.data();

        this.title = data.title;
        this.votes = data.votes;
    }
}