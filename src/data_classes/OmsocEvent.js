import Topic from "./Topic";

export default class OmsocEvent {
    constructor(doc){
        this.id = doc.id;
        const data = doc.data();
        this.topic = data.topic;
        this.description = data.description;
        this.venue = data.venue;
        this.scheduledForUnix = data.scheduledFor.seconds;
    }

    formatScheduledFor(detail=false){
        return Topic.formatDate(this.scheduledForUnix, detail);
    }
}
