import React from 'react';
//import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

function Spinner(props){
    return (
        <table style={{padding: '12px'}}>
            <tbody>
            <tr>
                <td style={{paddingRight:'18px'}}><CircularProgress thickness={5} size={74} /></td>
                { props.text && <td><p>{props.text}</p></td>}
            </tr>
            </tbody>
        </table>
    );
}

export default Spinner;