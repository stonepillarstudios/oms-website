import React from "react";
import {IconButton, Snackbar, SnackbarContent} from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';

export default class Snacky extends React.Component {

    //props: icon, text, open, onClose

    render() {

        const displayIcon = this.props.icon ? this.props.icon : <div/>;

        return (<Snackbar open={this.props.open} autoHideDuration={5000} onClose={this.props.onClose}
                          anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}>
            <SnackbarContent style={{backgroundColor: 'white'}} message={<div style={{display: 'table'}}>
                <div style={{display: 'table-cell'}}> {displayIcon} </div> <div style={{display: 'table-cell',
                verticalAlign: 'middle', paddingLeft: '18px'}}>{this.props.text}</div></div>}
                             action={[<IconButton onClick={this.props.onClose} key="close">
                                 <CloseIcon/></IconButton>]}/></Snackbar>);
    }

}