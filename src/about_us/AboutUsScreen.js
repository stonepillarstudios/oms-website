import React from "react";

export default class AboutUsScreen extends React.Component {

    render() {
        return (
            <div style={{margin: 32}}>
                <h1>About Us</h1>
                <p><strong>The UCT Open Mind Society</strong> strives to cultivate free thought among students.</p>

                <p>We host meet-ups on campus where students are encouraged to discuss topical issues
                    (e.g. GBV, meat-eating, AI ethics, social justice, BEE, PC culture).
                    Students from all faculties and walks of life are encouraged to participate and say their say.</p>

                <p>All topics are suggested by students on our website. The topics of discussion for our events are
                    either decided by student topic polls, or by direct selection from a representative student
                    committee.</p>

                <h2>Our purpose</h2>

                <p>The five founders of OMSoc had one overarching ideal at heart: promoting critical
                    discussion and topical engagement of prevalent societal issues among students.
                    Serving this purpose means getting people from different backgrounds in the same room to
                    speak with and understand each other. We specifically aim the society at students with
                    no formal debating experience (or interest) and we make an atmosphere of any-opinion-goes
                    clear at all events.</p>

                <p>While this may frustrate some, we resolved to always keep this paradigm standing. The purpose
                    of Omsoc events are not to reach consensus or to uncover some truth, but rather to have
                    students hear out each other's opinions, engage them critically, and leave with a
                    deeper empathy and mutual understanding of the topic at hand. Besides, the
                    debating society is always there for those that feel a need to win an argument.</p>

                <h3>Our history</h3>

                <img src={require("../assets/history/oweek-collage.jpg")} alt="The Omsoc founders at O-Week 2020"
                     style={{width: 642, maxWidth: '80vw', float: 'left', marginRight: 16}}/>

                <p>At the end of 2019 some polling was done to determine interest in such a society –
                    one that provides a platform for critical discussion and engagement with prevalent
                    day-to-day moral and political issues. Unsurprisingly there was a great deal of interest,
                    and this was evidenced by student participation in our first few events.</p>

                <p>As 2020 drew round, the founders relied on word-of-mouth and the occasional flyer to
                    serve as basis for Omsoc’s advertising and awareness campaign. Since signing up is free
                    and easy by virtue of their original website, they hoped this would quickly attract many
                    students to form part of the society. Nevertheless, sign-ups were rather slow to begin with.</p>

                <p>Through some wheeling and dealing by the founders, the UCT Philosophy department was made aware of and drawn
                    into the society's plans. They opted to assist in advertising by sending out an announcement to
                    all undergraduate philosophy students. This lead to the wildfire-spread of the society as idea among students,
                    and an extremely successful and controversial inaugural event.</p>

                <img src={require("../assets/history/event1.jpg")} alt="An ad for Omsoc's first ever event"
                     style={{width: 488, maxWidth: '80vw', float: 'left', margin: 16}}/>

                <p>And the rest, they say, is history.</p>

            </div>
        );
    }

}
