import React from "react";
import './VotingScreen.css';
import {LinearProgress} from "@material-ui/core";
import {analytics, firestore} from '../Firebase';
import VotingBallot from "../data_classes/VotingBallot";
//import Login from "../ubiquitous/Login";
import VotingComponent from "./VotingComponent";
import ResultsComponent from "./ResultsComponent";

export default class VotingScreen extends React.Component {

    constructor(props){
        super(props); //props: loggedInUser

        this.state = {
            display: 'LOADING', //states: LOADING, VOTE, RESULTS, CLOSED
            loadingProgress: 0,
            //loggedInUser: null
        };

        this.ballot = null;
    }

    componentDidMount() {
        setTimeout(()=>{ // get that loader going
            this.setState({loadingProgress: 23});
        }, 250);

        const self = this;
        //get ballot with deadline closest in the future to now.
        firestore.collection('voting').where('deadline', '>', new Date())
            .orderBy('deadline', 'asc').limit(1).get().then(snap => {
            //should only be one or zero
            let blt = null;
            snap.forEach(doc => {
                blt = new VotingBallot(doc);
            });

            if (!blt){
                self.setState({
                    loadingProgress: 100,
                    display: 'CLOSED'
                }); return;
            }//else...
            self.ballot = blt;

            self.setState({
                loadingProgress: 55
            });

            //get topics
            self.ballot.loadTopics().then(() => {
                self.setState({
                    loadingProgress: 85
                });

                //check if already voted
                if (self.props.loggedInUser) {
                    self.ballot.hasVoted(self.props.loggedInUser.uid).then(has=>{
                        if (has) {
                            self.setState({loadingProgress: 100, display: 'RESULTS'});
                        } else {
                            self.setState({loadingProgress: 100, display: 'VOTE'});
                        }
                    });
                }else{
                    self.setState({
                        loadingProgress: 100,
                        display: 'VOTE'
                    });
                }
            });
        });

        analytics.logEvent('screen_view', {'screen_name': 'Voting'});
    }

    render() {

        const onVote = () => {
            this.setState({
                display: 'RESULTS'
            });
        };

        return (<div id="vote-main">
            {this.state.display === 'LOADING' &&
            <div style={{margin: 'auto', marginTop: '45vh'}}><LinearProgress variant="buffer"
                value={this.state.loadingProgress} valueBuffer={this.state.loadingProgress}/></div>}
            {this.state.display === 'CLOSED' &&
            <div id="closed-block"><h2>Voting closed for now!</h2><p>We'll keep you informed of results and new polls.</p></div>}
            {this.state.display === 'VOTE' &&
            <div><VotingComponent ballot={this.ballot} loggedInUser={this.props.loggedInUser}
                onVote={onVote}/></div>}
            {this.state.display === 'RESULTS' &&
            <div><ResultsComponent ballot={this.ballot}/></div>}
        </div>)
    }
}
