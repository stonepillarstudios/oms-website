import React, {Component} from "react";
import EmailIcon from '@material-ui/icons/Email';
import Grid from '@material-ui/core/Grid';

export default class CommitteeScreen extends Component {

    constructor(props){
        super(props);

        this.comm = [
            ['george', 'rtnjan001', 'Chairperson', 'George Rautenbach'],
            ['lloyd', 'evrllo001', 'Vice-Chairperson', 'Lloyd Everett'],
            ['tsholo', 'mbxmok002', 'Operations Officer', 'Tsholofelo Mogoregi'],
            ['sibu', 'nbhsib001', 'Treasurer', 'Sibulele Nobhuzana'],
            ['cameron', 'jspcam002', 'Secretary', 'Cameron Joseph'],
        ].map(tuple => [require('../assets/omscomm/'+tuple[0]+'.jpg'),
            tuple[1]+"@myuct.ac.za", tuple[2], tuple[3]]);

        CommitteeScreen.customShuffle(this.comm);
    }

    static customShuffle(array) {
        let temp, randomIndex;

        for (let i = 2; i < 5; i++) {
            randomIndex = 2 + Math.round(Math.random() * 2);
            temp = array[i];
            array[i] = array[randomIndex];
            array[randomIndex] = temp;
        }
    }

    render() {

        return <div>
            <p style={{textAlign: 'center'}}>[<span style={{fontWeight: 'bold'}}>Note</span>: 2021 Exec TBA. Watch this space.]</p>
            <h1 style={{textAlign: 'center'}}>2020 Executive Committee</h1>

            <Grid container justify="center">
                {this.comm.map(member => <Grid item key={member[0]} style={{padding: 24, textAlign: 'center'}}>
                    <img src={member[0]} alt={member[3]} style={{maxWidth: '80vw', maxHeight: '30vh'}}/>
                    <p><span style={{fontSize: '2em', fontWeight: 'bolder'}}>{member[3]}</span><br/>
                        <span style={{fontSize: '1.4em'}}>{member[2]}</span><br/>
                        <div style={{display: 'flex', justifyContent:'center', alignItems: 'center'}}>
                            <a href={'mailto:'+member[1]} style={{paddingTop: 6}}><EmailIcon/></a>
                            <span>&nbsp;</span>
                            <a href={'mailto:'+member[1]} style={{fontSize: '1.2em'}}>{member[1]}</a>
                        </div>
                    </p>
                </Grid>)}
            </Grid>
        </div>
    }

}
