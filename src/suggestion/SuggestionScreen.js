import React from "react";
import {Paper, TextField, Button} from "@material-ui/core";
import User from "../data_classes/User";
import PropTypes from 'prop-types';
import {analytics, firestore} from "../Firebase";

export default class SuggestionScreen extends React.Component {

    static propTypes = {
        loggedInUser: PropTypes.instanceOf(User), //optional
    };

    constructor(props){
        super(props);

        this.state = {
            submitted: false,
            error: null,
            topic: '',
            totalTopics: null,
        };

        this.onSubmitClick = this.onSubmitClick.bind(this);
    }

    onSubmitClick(){
        if (this.state.topic.length < 10){
            this.setState({
                error: 'Give a bit more detail'
            }); return;
        }

        firestore.collection('topics').add({
            title: this.state.topic,
            submittedOn: new Date(),
            suggester: this.props.loggedInUser.uid,
        }).then(x=>{
            this.setState(state =>({
                submitted: true,
                totalTopics: state.totalTopics+1, //visual update for the laymen
            }));
        });

        analytics.logEvent('suggestion');
    }

    componentDidMount() {
        firestore.collection('aggs').doc('topics').get().then(doc => {
            this.setState({
                totalTopics: doc.data().total,
            });
        });
    }

    render(){

        const onChange = event => {
            this.setState({
                topic: event.target.value,
            });
        };

        const loginStatus = this.props.loggedInUser ? this.props.loggedInUser.role === 'uctcom' ?
            'GOOD' : 'NOTUCT' : 'LOGGEDOUT';

        return (<div style={{paddingTop: 24}}>
            <Paper className="wide_paper">
                {!this.state.submitted && <div>
                    <h3>Suggest a topic</h3>
                    {loginStatus === 'GOOD' && <div>
                        <p>All our meetings are themed from topics suggested by students. </p>
                        <TextField label="Suggestion" onChange={onChange} variant="outlined" fullWidth
                                   error={this.state.error !== null} helperText={this.state.error}/>
                        <Button variant="contained" onClick={this.onSubmitClick} color="secondary"
                                disabled={loginStatus !== 'GOOD'} style={{marginTop: 18}}>Submit</Button>
                    </div>}
                    {loginStatus === 'LOGGEDOUT' && <div>
                        <p>All our meetings are themed from topics suggested by students. </p>
                        <u><code>Please log in to make a suggestion.</code></u>
                    </div>}
                    {loginStatus === 'NOTUCT' && <div>
                        <u><code>Topic suggestion is only available to UCT students &amp; staff. If you have a UCT
                            &nbsp;account, please log out and log in to it.</code></u>
                    </div>}

                </div>}
                {this.state.submitted && <div>
                    <p>Your topic has been added to the suggestions. Look out for it in a future topic poll!</p>
                </div>}
                {this.state.totalTopics &&
                    <p style={{textAlign: 'center'}}>Total topics suggested: {this.state.totalTopics}</p>}

            </Paper>
        </div>)
    }

}
