import React from "react";
import {Paper, Divider} from "@material-ui/core";
import Spinner from "../ubiquitous/Spinner";
import {firestore} from "../Firebase";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Snacky from "../ubiquitous/Snacky";
import DoneIcon from "@material-ui/icons/Done";
import ErrorIcon from "@material-ui/icons/Error";

export default class EmailLister extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            subbers: null,
            edtEmail: '',
            clearing: false,
            result: null,
            snackIcon: null,
        };

        this.onClearUCTClick = this.onClearUCTClick.bind(this);
        this.onClearNonClick = this.onClearNonClick.bind(this);
        this.onReceiverUpdate = this.onReceiverUpdate.bind(this);
    }

    componentDidMount() {
        firestore.collection('subscribers').where('vula', '==', false).get().then(docs => {
            const subbers = [];
            docs.forEach(doc => {
                subbers.push([doc.id, doc.data().email])
            });
            this.setState({subbers: subbers});
        });
        firestore.collection('config').doc('subscribers_notifications').get().then(doc => {
            this.setState({edtEmail: doc.data().email});
        })
    }

    async onClearUCTClick(){
        this.setState({clearing: true});
        for (let subber of this.state.subbers) {
            if (subber[1].endsWith('uct.ac.za'))
                await firestore.collection('subscribers').doc(subber[0]).update({vula: true});
        }
        this.setState({clearing: false, subbers: null});
        this.componentDidMount();
    }

    async onClearNonClick(){
        this.setState({clearing: true});
        for (let subber of this.state.subbers) {
            if (!subber[1].endsWith('uct.ac.za'))
                await firestore.collection('subscribers').doc(subber[0]).update({vula: true});
        }
        this.setState({clearing: false, subbers: null});
        this.componentDidMount();
    }

    async onReceiverUpdate(){
        firestore.collection('config').doc('subscribers_notifications').update({
            email: this.state.edtEmail
        }).then(()=>{
            this.setState({result: 'Contact updated.', snackIcon: <DoneIcon/>});
        }, (err)=>{
            console.log(err);
            this.setState({result: 'Contact could not be updated.', snackIcon: <ErrorIcon/>});
        })
    }

    render(){
        const uctsubbers = []; const nonuctsubbers = [];

        if (this.state.subbers) {
            this.state.subbers.forEach(subber => {
                if (subber[1].endsWith('uct.ac.za'))
                    uctsubbers.push(subber[1]);
                else
                    nonuctsubbers.push(subber[1]);
            });
        }

        return (<div>
            <Paper style={{padding: '24px'}}>
                <h2>Subscribers</h2>
                {!this.state.subbers &&
                <Spinner text="Loading subscribers..."/>}
                {this.state.subbers &&
                <div><p>Subscribers to process: {this.state.subbers.length}</p>
                    <h4>UCT to be added to Vula</h4>
                    {uctsubbers.length === 0 && <code>None</code>}
                    {uctsubbers.length > 0 && <div>
                        <p>{uctsubbers.map(subber=>{
                            return <span key={subber}>{subber}<br/></span>
                        })}</p>
                        <Button variant="contained" color="primary" onClick={this.onClearUCTClick}
                            disabled={this.state.clearing}>Clear</Button>
                    </div>
                    }

                    <h4>Non-UCT</h4>
                    {nonuctsubbers.length === 0 && <code>None</code>}
                    {nonuctsubbers.length > 0 && <div>
                        <p>{nonuctsubbers.map(subber=>{
                            return <span key={subber}>{subber},<br/></span>
                        })}</p>
                        <Button variant="contained" color="primary" onClick={this.onClearNonClick}
                                disabled={this.state.clearing}>Clear</Button>
                    </div>}

                    <Divider style={{marginTop: 20}}/>
                    <h3>Notifications</h3>
                    <p>Once a day a script checks for new subscribers and if there are, sends
                    an email to the below address.</p>

                    <Grid container style={{marginTop: 24}}>
                        <Grid item xs={8}>
                            <form>
                                <TextField id="edt_email" label="Email" onChange={event => {
                                    this.setState({edtEmail: event.target.value});
                                }} margin="normal" fullWidth variant="outlined" value={this.state.edtEmail}/>
                            </form>
                        </Grid>
                        <Grid item xs={4} style={{paddingTop: '25px', paddingLeft: 12}}>
                            <Button variant="contained" onClick={this.onReceiverUpdate} color="primary"
                                    disabled={!this.state.edtEmail}>Update</Button>
                        </Grid>
                    </Grid>
                </div>}
            </Paper>

            <Snacky open={!!this.state.result} onClose={()=>{this.setState({result: null})}}
                    icon={this.state.snackIcon} text={this.state.result}/>
        </div>);
    }

}
