import React from "react";
import {Paper, TextField} from "@material-ui/core";
import {KeyboardDateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {Button} from "@material-ui/core";
import Snacky from "../ubiquitous/Snacky";
import DoneIcon from '@material-ui/icons/Done';
import {firestore} from "../Firebase";

export default class EventSummaryMan extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            topic: '',
            scheduled: null,
            summary: '',
            submitting: false,
            result: null,
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(){
        this.setState({submitting: true});
        firestore.collection('summaries').add({
            topic: this.state.topic,
            scheduled: new Date(this.state.scheduled),
            summary: this.state.summary.trim().split('\n'), //array-ify
        }).then(()=>{
            this.setState({submitting: false, result: 'Summary posted.'});
        });
    }

    render() {

        const onTextChange = name => e => {
            this.setState({
                [name]: e.target.value
            });
        };
        const onDateChange = (e, res) => {
            this.setState({
                scheduled: res,
            })
        };

        return <div>
            <Paper style={{padding: 24}}>
                <h2>New Event Summary</h2>
                <TextField label="Topic" fullWidth onChange={onTextChange('topic')}/>
                <div style={{marginTop: 20, marginBottom: 12}}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} >
                        <KeyboardDateTimePicker label="Date and time" format="yyyy-MM-dd HH:mm"
                                                onChange={onDateChange} value={this.state.scheduled}/>
                    </MuiPickersUtilsProvider></div>
                <TextField label="Summary" fullWidth multiline rows="10" onChange={onTextChange('summary')}
                    style={{marginBottom: 24}}/>
                <Button variant="contained" onClick={this.onSubmit} disabled={this.state.submitting}>Submit</Button>
            </Paper>

            <Snacky open={!!this.state.result} onClose={()=>{this.setState({result: null})}}
                    icon={<DoneIcon/>} text={this.state.result}/>
        </div>
    };

}
