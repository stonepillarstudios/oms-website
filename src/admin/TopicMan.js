import React from "react";
import {
    Checkbox,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Button,
    Divider,
} from "@material-ui/core";
import Spinner from "../ubiquitous/Spinner";
import {firestore} from "../Firebase";
import Topic from "../data_classes/Topic";
import Grid from "@material-ui/core/Grid";
import {KeyboardDateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';
import InfoIcon from '@material-ui/icons/Info';
import Snacky from "../ubiquitous/Snacky";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";

export default class TopicMan extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            topics: null,
            busy: true,
            checked: {},
            selDate: new Date().toLocaleDateString('en-ZA', {
                day: 'numeric', month: 'numeric', year: 'numeric'
            }) +' '+ new Date().toLocaleTimeString('en-ZA', {
                minute: 'numeric', hour: 'numeric'
            }),

            showNames: false,
        };

        this.onDeleteClick = this.onDeleteClick.bind(this);
        this.refreshTopics = this.refreshTopics.bind(this);
        this.onScheduleVoteClick = this.onScheduleVoteClick.bind(this);
        this.handleShowNames = this.handleShowNames.bind(this);
    }

    componentDidMount() {
        this.refreshTopics();
    }

    refreshTopics(){
        firestore.collection('topics').orderBy('submittedOn', 'desc').get().then(snap => {
            const parsed = [];
            snap.forEach(doc => {
                parsed.push(new Topic(doc));
            });
            this.setState({
                topics: parsed,
                busy: false,
                checked: {},
            });
        });
    }

    async onDeleteClick(){
        this.setState({busy: true});
        const sels = this.getSelected();
        for (let top of sels) {
            await firestore.collection('topics').doc(top).delete();
        }
        this.refreshTopics();
    }

    onScheduleVoteClick(){
        this.setState({busy: true});
        const dline = new Date(this.state.selDate);

        firestore.collection('voting').add({
            deadline: dline,
        }).then(doc => {
            const ballot = doc.id;
            firestore.runTransaction(trans => {
                const ref = firestore.collection('voting/'+ballot+'/topics');

                for (let topic of this.state.topics) {
                    if (topic.id in this.state.checked && this.state.checked[topic.id])
                        trans.set(ref.doc(topic.id), {
                            title: topic.title,
                            votes: 0,
                        });
                }
                return Promise.resolve();
            }).then(x => {
                this.setState({busy: false, result: 'Vote successfully scheduled.'});
            });
        });

    }

    somethingSelected(){
        for (let k in this.state.checked) {
            if (this.state.checked.hasOwnProperty(k) && this.state.checked[k])
                return true;
        }
        return false;
    }
    getSelected(){
        const sel = [];
        for (let k in this.state.checked) {
            if (this.state.checked.hasOwnProperty(k) && this.state.checked[k])
                sel.push(k);
        }
        return sel;
    }

    async handleShowNames(e, checked){
        this.setState({showNames: checked});

        if (checked){
            for (const topic of this.state.topics){
                if (topic.suggesterName === '') {
                    await topic.getSuggesterName();
                    this.setState(this.state);
                }
            }
        }
    }

    render() {
        //when calling this thing it generates a function that updates the specified part of the state. How cool is that?
        const onCheck = name => (e, checked) => {
            this.setState(state => {
                return {
                    ...state,
                    checked: {
                        ...state.checked,
                        [name]: checked,
                    },
                };
            });
        };

        const onDateChange = (e, res) => {
            this.setState({
                selDate: res,
            })
        };

        return (<Paper style={{padding: '24px'}}>
            <h2>Topic ideas</h2>
            {!this.state.topics &&
            <Spinner text="Loading topics..."/>}
            {this.state.topics &&
            <div>
                <FormControlLabel
                    control={
                        <Switch onChange={this.handleShowNames} value={this.state.showNames} />
                    }
                    label="Show Suggesters"
                />
                <p>Total submitted topics: {this.state.topics.length}</p>
                <Table>
                    <TableHead><TableRow>
                        <TableCell/>
                        <TableCell>Topic</TableCell>
                        <TableCell>Submitted On</TableCell>
                        {this.state.showNames && <TableCell>Suggester</TableCell>}
                    </TableRow></TableHead>
                    <TableBody>
                        {this.state.topics.map(topic => (<TableRow key={topic.id}>
                            <TableCell><Checkbox value={topic.id} onChange={onCheck(topic.id)}/></TableCell>
                            <TableCell>{topic.title}</TableCell>
                            <TableCell>{topic.formatSubmittedOn()}</TableCell>
                            {this.state.showNames && <TableCell>{topic.suggesterName}</TableCell>}
                        </TableRow>))}
                    </TableBody>
                </Table>
                <h4>With selected ({this.getSelected().length}):</h4>
                <Button variant="contained" color="secondary" onClick={this.onDeleteClick}
                        disabled={this.state.busy || !this.somethingSelected()}>Delete</Button>
                <Divider style={{marginTop: '24px', marginBottom: '24px'}}/>
                <Grid container disabled={true}>
                    <Grid item xs={12} style={{display: 'flex'}}>
                        <div >
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDateTimePicker label="Voting deadline" format="yyyy-MM-dd HH:mm"
                                onChange={onDateChange} value={this.state.selDate}/>
                            </MuiPickersUtilsProvider>
                        </div>
                        <div style={{padding: '8px'}}>
                            <Button variant="contained" onClick={this.onScheduleVoteClick} color="primary"
                                    disabled={this.state.busy || this.getSelected().length < 2}>Schedule Vote</Button>
                        </div>
                    </Grid>
                    <p>The website automatically runs the vote scheduled closest in the future. You can schedule
                        multiple votes and the website will automatically run them as time progresses.</p>
                </Grid>
            </div>}

            <Snacky open={!!this.state.result} onClose={()=>this.setState({result: null})}
                icon={<InfoIcon/>} text={this.state.result}/>

        </Paper>)
    }

}
