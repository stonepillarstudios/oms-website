import {
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Button,
    Divider,
} from "@material-ui/core";
import React from "react";
import Spinner from "../ubiquitous/Spinner";
import {firestore} from "../Firebase";
import Snacky from "../ubiquitous/Snacky";
import DoneIcon from '@material-ui/icons/Done';
import ErrorIcon from '@material-ui/icons/Error';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";

export default class AdminMeta extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            admins: null,
            result: null,
            snackIcon: null,
            busy: true,
            edtEmail: '',
        }

        this.refreshAdmins = this.refreshAdmins.bind(this);
        this.onKickAdminClick = this.onKickAdminClick.bind(this);
        this.onGivePerms = this.onGivePerms.bind(this);
    }

    componentDidMount() {
        this.refreshAdmins();
    }

    refreshAdmins(){
        firestore.collection('users').where('role', '==', 'admin').get()
            .then(snap => {
                const admins = [];
                snap.forEach(admin => {
                    admins.push({...admin.data(), id: admin.id});
                });
                this.setState({admins: admins, busy: false});
            })
    }

    onKickAdminClick(admin){
        this.setState({busy: true});
        firestore.collection('users').doc(admin.id).update({
            role: admin.email.endsWith('uct.ac.za') ? 'uctcom' : 'none'
        }).then(()=>{
            this.setState({result: 'Admin kicked.', snackIcon: <DoneIcon/>, busy: false});
            this.refreshAdmins();
        }, (err)=>{
            console.log(err);
            this.setState({result: 'Failed to kick admin.', snackIcon: <ErrorIcon/>, busy: false});
        });
    }

    onGivePerms(){
        this.setState({busy: true});
        firestore.collection('users').where('email', '==', this.state.edtEmail).get()
            .then(snap => {
                if (snap.empty)
                    this.setState({result: 'Could not find user account.', snackIcon: <ErrorIcon/>, busy: false});
                else{
                    // update perms
                    firestore.collection('users').doc(snap.docs[0].id).update({
                        role: 'admin'
                    }).then(()=>{
                        this.setState({result: 'Admin added.', snackIcon: <DoneIcon/>, busy: false});
                        this.refreshAdmins();
                    }, (err)=>{
                        console.log(err);
                        this.setState({result: 'Failed to add admin.', snackIcon: <ErrorIcon/>, busy: false});
                    })
                }
            });
    }

    render() {

        const onKickAdminClick = (id) => () => {
            this.onKickAdminClick(id);
        }

        return (
            <div>
                <Paper style={{padding: '24px'}}>
                    <h2>Manage admins</h2>
                    {!this.state.admins &&
                    <Spinner text="Loading admins..."/>}

                    {this.state.admins &&
                    <div>
                        <Table>
                            <TableHead><TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell>Email</TableCell>
                                <TableCell>Action</TableCell>
                            </TableRow></TableHead>
                            <TableBody>
                                {this.state.admins.map(admin => <TableRow key={admin.id}>
                                    <TableCell>{admin.displayName}</TableCell>
                                    <TableCell>{admin.email}</TableCell>
                                    <TableCell><Button variant="contained" color="secondary"
                                        onClick={onKickAdminClick(admin)} disabled={this.state.busy}>Kick</Button></TableCell>
                                </TableRow>)}
                            </TableBody>
                        </Table>
                    </div>}

                    <Divider/>
                    <h3>Give admin permissions</h3>
                    <p>To add someone as an admin, have them login to the system with a <strong>non-UCT</strong>
                        &nbsp;account. This will register them to the system. Then add their email below, and hit
                        "elevate" to make them admin. <br/>Note: because of implementation issues, one account can't
                        simultaneously be a UCT member (with e.g. voting privileges) and an admin. Use two accounts.</p>
                    <Grid container style={{marginTop: 24}}>
                        <Grid item xs={8}>
                            <form>
                                <TextField id="edt_email" label="Email" onChange={event => {
                                    this.setState({edtEmail: event.target.value});
                                }} margin="normal" fullWidth variant="outlined" value={this.state.edtEmail}/>
                            </form>
                        </Grid>
                        <Grid item xs={4} style={{paddingTop: '25px', paddingLeft: 12}}>
                            <Button variant="contained" onClick={this.onGivePerms} color="primary"
                                    disabled={!this.state.edtEmail || this.state.busy}>Elevate</Button>
                        </Grid>
                    </Grid>
                </Paper>

                <Snacky open={!!this.state.result} onClose={()=>{this.setState({result: null})}}
                        icon={this.state.snackIcon} text={this.state.result}/>
            </div>
        );
    }

}
